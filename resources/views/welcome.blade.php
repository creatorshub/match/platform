@php
$baseUrl = "https://creatorshub-community.ams3.digitaloceanspaces.com/assets/landing-page"
@endphp
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>Collaboration for Creators | CreatorsHub Match</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ $baseUrl }}/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ $baseUrl }}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ $baseUrl }}/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ $baseUrl }}/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ $baseUrl }}/css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
</head>
<body data-smooth-scroll-offset="77">
<div class="nav-container"> </div>
<div class="main-container">
    <section class="imageblock switchable feature-large height-100">
        <div class="imageblock__content col-lg-6 col-md-4 pos-right">
            <div class="background-image-holder"> <img alt="image" src="{{ $baseUrl }}/img/inner-3.jpg"> </div>
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-lg-5 col-md-7">
                    <h1>Launching Soon</h1> <span class="h2 countdown color--primary" data-date="09/25/2018" data-fallback-text="Getting ready"></span>
                    <p class="lead">We're preparing to launch soon — hit the form below and we'll let you know when we're live.</p>
                    <form action="https://plsk.us17.list-manage.com/subscribe/post?u=85ff7be8fc3194c17722870d6&amp;id=209c1cf0ef" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your email address.">
                        <div class="row">
                            <div class="col-12"> <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Email Address"> </div>
                            <div class="col-12"> <button type="submit" class="btn btn--primary type--uppercase">Notify Me</button> </div>
                            <div class="col-12"> <span class="type--fine-print">By signing up, you agree to our <a href="https://www.creatorshub.net/privacy/">Privacy Policy</a></span> </div>
                            <div style="position: absolute; left: -5000px" aria-hidden="true"> <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value=""> </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <footer class="space--sm footer-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3 col-xs-6">
                    <h6 class="type--uppercase">Creatorshub</h6>
                    <ul class="list--hover">
                        <li><a href="https://www.creatorshub.net/index.php?app=forums&module=forums&controller=index"
                               target="_self">Home</a></li>
                        <li><a href="https://www.creatorshub.net/privacy/" target="_self">Privacy Policy</a></li>
                        <li><a href="https://www.creatorshub.net/legal/imprint" target="_self">Legal Notice</a></li>
                        <li><a href="https://www.creatorshub.net/contact/" target="_self">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3 col-xs-6">
                    <h6 class="type--uppercase">Our Apps</h6>
                    <ul class="list--hover">
                        <li><a href="https://music.creatorshub.net/" target="_self">Music Library</a></li>
                        <li><a href="https://match.creatorshub.net/" target="_self">Collaboration Finder</a></li>
                        <li><a href="https://podcasts.creatorshub.net/" target="_self">Podcaster Service</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3 col-xs-6">
                    <h6 class="type--uppercase">Communities</h6>
                    <ul class="list--hover">
                        <li>
                            <a href="https://www.creatorshub.net/index.php?app=forums&module=forums&controller=forums&id=1"
                               target="_self">YouTube</a></li>
                        <li>
                            <a href="https://www.creatorshub.net/index.php?app=forums&module=forums&controller=forums&id=42">Twitch</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3 col-xs-6">
                    <h6 class="type--uppercase">Follow Us</h6>
                    <ul class="list--hover">
                        <li>
                            <a href="//twitter.com/creatorshub" target="_blank" rel="noreferrer noopener nofollow">
                                <i class="socicon socicon-twitter icon icon--xs"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <span class="type--fine-print">© 2018 CreatorsHub</span>
                </div>
                <div class="col-sm-6 text-right text-left-xs">
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{ $baseUrl }}/js/jquery-3.1.1.min.js"></script>
<script src="{{ $baseUrl }}/js/parallax.js"></script>
<script src="{{ $baseUrl }}/js/countdown.min.js"></script>
<script src="{{ $baseUrl }}/js/smooth-scroll.min.js"></script>
<script src="{{ $baseUrl }}/js/scripts.js"></script>

</body>

</html>